# README #

This is a few notes on things that I did while creating the page, to clarify the process or decisions I made while preapring the site. 

## Overview
I tried to adhere more or less to the 4 hour limit, with changes pushed to Bitbucket at the end of each hour to demonstrate the process, where I had got to etc.

First hour - looked at the layout, identified common components across the site (headings, fonts, colour, layout sizes), and started preparing the HTML file for the site. My main focus in that process was considering what was required for the site and creating links in the head. I also prepared a folder structure and set up style.scss and style.css files in the appropriate folder.  Also resized all oversize images to reduce their size so that they weren't slowing page loading. (120dpi, 80 quality jpeg, reduced width/height to cut down the file size).

Second hour - Continued filling out the site sections to create content to work with. Started adding global styles to the site as variables in SCSS (h2, h3, colors, font families etc). Started to repared this document.

Third hour - Continue creating HTML structure. Added in media queries to ensure any necessary tweaks made across different size screens.

Fourth hour - Carousel, started on navigation bar. 

In total I probably spent around 4.5 to 5 hours.

** Ran out of time - Didn't get to adding in the Google map, although I'm familiar with that process. Didn't have an opportunty to work on improving page load time and double checking any potential issues for accessbility.  Ran out of time to do the pink line below the headings.

## CDNs

## Bootstrap
I've used the Bootstrap framework to simplify the creation of the banner carousel, the grid structures in the Services and News sections and to ensure responsiveness across different size devices.

## Google fonts
I selected the fonts that I felt were the necessary ones only (rather than bringing in the entire font family). This should help with managing page load time.


## CAROUSEL
The carousel uses the Bootstrap framework based on W3C standard, with a CSS animation transition (only supported for IE8+).  The background of the section displays the first image as default, which then becomes the fall back if the Carousel is not supported.

## SVG
There are various options that I had identified of for including SVG elements in an accessible way in HTML5,in particular with a <use> tag, which allows for more comprehensive description of the image. However, I felt it was simplest to use as an <img> with an alt attribute for the icon svg files, as this was probably as much as was necessary.  This was also the option that was compatible with Internet Explorer 11.

