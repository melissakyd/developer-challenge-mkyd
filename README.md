# README #

We want to know more about the way you work. We want to know what your strengths and weakness are. We want to know how fussy you are about your code.

And the best way to find this out is to give you a challenge.

## Details

For this challenge you will be required to code a one-page website, using layout.jpg as your guide.

Please spend a maximum of four hours working on it.

We don’t expect everyone to complete everything in the time allocated, so use it as a chance to show how you prioritise. Think of this as an opportunity to show us what you can do. If there is anything you can’t do, or run out of time to do, leave a comment in the markup to mention why you left it left out.

- Fork this repository
- Commit your changes and push them to Bitbucket
- Create a pull request in Bitbucket so we can review your work

If you have trouble with using git and BitBucket, please zip up your project and email a dropbox link to us.

### Considerations

- Browser requirements are desktop IE11+, latest versions of Chrome, Firefox, Safari and Edge.
- The hero image should take up 90% of the browsers height and use a carousel to cycle through the three images supplied
- The design is a guide only, the text does not have to match exactly, niether does the design need to be pixel perfect.
- This site is responsive with a minimum size of 320px and at least should support iPad portrait and landscape.
- The font used is Railway and is available as a google font.
- The designer hasn’t created mobile designs and would like you to recommend the solution directly in the HTML so they can review it after you’ve built it.
- If you're using SASS/SCSS/LESS, please include your construction files as well as the output HTML.
- Comment frequently to talk us through what you’ve done – like you’d normally do when writing code.

Above all, as mentioned above - we're not expecting a completed masterpiece. We're looking for your attention to detail, semantic goodness, accessibility, performance and to see how you tackle different problems.

### Accessibility
Accessibility is more than making sure the site works with screen readers and assistive technology. Accessibility means making sure people can access the site anywhere, on any device, regardless of their physical abilities. Any site should:

- adapt to fit the device it's being viewed on
- work on modern browsers (even when JavaScript is disabled), with progressive enhancement to provide additional functionality
- use semantic markup
- have clearly labelled elements, regardless of whether this is achieved using text, alt text or [WAI-ARIA roles](http://www.w3.org/TR/wai-aria/roles)
- comply to [WCAG 2.0](http://www.environment.sa.gov.au/documentation/design-considerations.html) Level A (as a minimum)
- use microdata where possible.

### Performance

Slow websites are frustrating. [A 2009 study found that 47% of people expected a webpage to load in less than two seconds](http://www.akamai.com/html/about/press/releases/2009/press_091409.html). This won't always be possible, especially on slow connections or for complex pages, but optimising the content and the way it is delivered will make a big difference.

- optimise the critical rendering path, so content is displayed as soon as possible
- resize and optimise images, and save them in an appropriate format
- concatenate and minify CSS and JavaScript, and consider including it inline, where appropriate
- load common JavaScript libraries from a Content Delivery Network (CDN) with locally hosted fallback

Google's [Pagespeed Insights](https://developers.google.com/speed/pagespeed/insights/) provide a wealth of information and advice in this area, but [WebPagetest.org](http://www.webpagetest.org/) provides more detailed test results.
